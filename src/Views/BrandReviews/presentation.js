import React from 'react'
import './style.css'

import SlideShow from '../../Components/SlideShow'

class Presentation extends React.Component{
    render(){
        return(
            <div>
                <SlideShow slides={this.props.brands} />
                {this.props.reviews}
            </div>
        )
    }
}

export default Presentation