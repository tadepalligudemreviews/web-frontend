import React from 'react'
import Presentation from './presentation'

import ReviewCard from '../../Components/ReviewCard'

class Container extends React.Component{
    state = {}
    constructor(props){
        super(props)
        // console.log(this.props)
        const id = this.props.history.location.pathname.split('/').slice(-1)[0]
        console.log(id)
        this.state.id = id
        this.state.brands = this.props.middleware.state.brands.filter(b => b.raw.id === id)
        // if(this.props.middleware.state.auth.currentUser){
            this.props.middleware.getReviews(id).then(res => {
                this.state.data = res

                this.state.reviews = this.generateBrandsJSX(res)
                // console.log(this.state.reviews)
                this.setState({
                    update : true
                })
            }).catch(err => console.log(err))   
        // }
    }

    generateBrandsJSX = (reviewList) => {
        const jsx = reviewList.map(r => {
            const data = {
                photoUrl: "https://microhealth.com/assets/images/illustrations/personal-user-illustration-@2x.png",
                name : r.name,
                timestamp : r.meta.timestamp,
                comment : r.comment,
                rating : r.rating+1
            }

            return (
                <ReviewCard middleware={this.props.middleware} data={data}/>
            )
        })
        return jsx
    }
    
    componentDidMount = () => {

    }

    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
            />
        )
    }
}

export default Container