import React from 'react'
import Presentation from './presentation'

class Container extends React.Component{
    state = {}
    formSubmitted = false
    constructor(props){
        super(props)
        if(this.props.middleware.state.auth.currentUser){

            this.state = {
                operation : this.showPostReviewButton()
            }
        }
    }
    
    componentDidMount = () => {

    }

    showPostReviewButton = () => {
        return(
            <div className="text-center row m-3">
                <div className="col">
                    <button className="btn btn-outline-success btn-lg" disabled={this.formSubmitted}>UPDATE</button>
                </div>
                <div className="col">
                    <button className="btn btn-outline-warning btn-lg" type="reset" disabled={this.formSubmitted}>RESET</button>
                </div>

            </div>
        )
    }

    validate = (e) => {
        let flag = 1

        if(e){
            if ((e.target.name === "name" || e.target.name === "category") && !e.target.value.match(/^[A-z\s]+$/)){
                this.setState({
                    name_error: "INVALID NAME",
                    category_error : "INVALID CATEGORY"
                })
                flag = 0
            }

            if(e.target.name === "banner"){
                this.setState({
                    banner : e.target.files[0]
                })
            }
            this.formSubmitted = false
            return flag
        }

        if(this.state.name)
        if(!this.state.name.match(/^[A-z\s]+$/)){
            this.setState({
                name_error : "INVALID NAME"
            })
            flag = 0
        }

        if(this.state.category)
        if(!this.state.category.match(/^[A-z\s]+$/)){
            this.setState({
                category_error : "INVALID CATEGORY"
            })
            flag = 0
        }

        if (!this.state.banner) {
            this.setState({
                banner_error: "PLEASE PROVIDE PRMOTION BANNER"
            })
            flag = 0
        }
        this.formSubmitted = false
        return flag
    }

    update = (e) => {
        this.setState({
            [e.target.name] : e.target.value,
            name_error : "",
            category_error : "",
            banner_error : "",
            rating_error : "",
            error : ""
        })
        this.validate(e)
    }

    submit = (e) => {
        e.preventDefault();
        this.formSubmitted = true
        if(!this.validate())
            return false;
        this.formSubmitted = true
        const data = {
            name : this.state.name || "---",
            category : this.state.category  || "---",
            banner : this.state.banner || "---",
            website : this.state.website || ("/"+this.state.name)
        }
        this.props.middleware.addBrand(data,(response) => {
            this.formSubmitted = false
            if(response.code){
                this.setState({
                    error : response.message
                })
            }else{
                this.setState({
                    error : "COMPLETED"
                })
            }
        })
    }

    loginWithGoogle = () => {
        this.props.middleware.loginWithGoogle((response) => {
            if(response.code)
                return
            this.setState({
                operation : this.showPostReviewButton()
            })      
        })
    }

    showLoginButton = () => {
        return(
            <img 
                alt="google-signin-button" 
                src="../../../images/btn_google_signin_dark_normal_web@2x.png"
                onClick={this.loginWithGoogle}
            />
        )
    }

    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                update={this.update}
                submit={this.submit}
                formSubmitted={this.formSubmitted}
            />
        )
    }
}

export default Container