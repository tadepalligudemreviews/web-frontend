import React from 'react'
import './style.css'

import Input from '../../Components/Input'
import Signin from '../../Components/Signin'

import Page404 from '../../Components/404'

class Presentation extends React.Component{
    render(){
        const email = this.props.middleware.state.auth.currentUser.email
        if (email !== "gopinadh5g7@sasi.ac.in" && email !== "naveen.loveall@gmail.com")
        return(
            <Page404/>
        )
        else
        return(
            <div className="container default-review-card my-5">
                <div className="card">
                    <div>{this.props.error}</div>
                    <div className="card-body">
                        <form onSubmit={this.props.submit}>
                            <Input inputName="Brand Name" error={this.props.name_error} name="name" onChange={this.props.update} disabled={this.props.formSubmitted}/>
                            <Input inputName="Brand Category" error={this.props.category_error} name="category" onChange={this.props.update} disabled={this.props.formSubmitted}/>
                            <Input inputName="Brand Website" name="website" onChange={this.props.update} disabled={this.props.formSubmitted}/>
                            <Input inputName="Brand Banner" className="btn btn-outline-danger" error={this.props.banner_error} name="banner" type="file" onChange={this.props.update} disabled={this.props.formSubmitted}/>
                            <Signin middleware={this.props.middleware} disabled={this.props.formSubmitted}/>
                            {this.props.operation}
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Presentation