import React from 'react'
import Presentation from './presentation'

class Container extends React.Component{
    state = {
        name : "",
        mobile : "",
        email : "",
        rating : -1
    }
    constructor(props){
        super(props)
        if(this.props.middleware.state.auth.currentUser){
            this.state.operation = this.showPostReviewButton()
        }
        this.state.options = this.generateBrandsJSX(this.props.middleware.state.brands)
    }

    generateBrandsJSX = (brandList) => {
        console.log(brandList)
        const jsx = brandList.filter(b => b.raw.id ? true : false).map(b => {
            return (
                <option key={b.raw.id} value={b.raw.id}>{b.raw.name} ({b.raw.category})</option>
            )
        })
        jsx.push((
            <option key="null_id" value="">---</option>
        ))
        return jsx
    }
    
    componentDidMount = () => {

    }

    showPostReviewButton = () => {
        return(
            <div className="text-center row m-3">
                <div className="col">
                    <button type="submit" className="btn btn-outline-success btn-lg" disabled={this.formSubmitted}>POST REVIEW</button>
                </div>
                <div className="col">
                    <button className="btn btn-outline-warning btn-lg" type="reset" disabled={this.formSubmitted}>RESET</button>
                </div>

            </div>
        )
    }

    validate = (e) => {
        let flag = 1

        if(e){
            if (e.target.name === "name" && !e.target.value.match(/^[A-z\s]+$/)){
                this.setState({
                    name_error: "INVALID NAME"
                })
                flag = 0
            }

            if (e.target.name === "email" && !e.target.value.match(/^.+@.+\..+$/)) {
                this.setState({
                    email_error: "INVALID EMAIL"
                })
                flag = 0
            }

            if (e.target.name === "mobile" && !e.target.value.match(/^[0-9]{10}$/)) {
                this.setState({
                    mobile_error: "INVALID MOBILE NUMBER"
                })
                flag = 0
            }
            return flag
        }

        if(!this.state.name.match(/^[A-z\s]+$/)){
            this.setState({
                name_error : "INVALID NAME"
            })
            flag = 0
        }

        if (!this.state.email.match(/^.+@.+\..+$/)) {
            this.setState({
                email_error: "INVALID EMAIL"
            })
            flag = 0
        }

        if (!this.state.mobile.match(/^[0-9]{10}$/)) {
            this.setState({
                mobile_error: "INVALID MOBILE NUMBER"
            })
            flag = 0
        }

        if(this.state.rating < 0){
            this.setState({
                rating_error : "PLEASE PROVIDE RATING"
            })
            flag = 0
        }

        if (!this.state.comment) {
            this.setState({
                rating_error: "PLEASE PROVIDE COMMENT"
            })
            flag = 0
        }

        if (!this.state.brand) {
            this.setState({
                brand_error: "PLEASE SELECT A BRAND"
            })
            flag = 0
        }

        return flag
    }

    update = (e) => {
        this.setState({
            [e.target.name] : e.target.value,
            name_error : "",
            email_error : "",
            mobile_error : "",
            rating_error : "",
            brand_error : ""
        })
        this.validate(e)
        // console.log(this.state)
    }

    submit = (e) => {
        e.preventDefault()
        console.log(this.state)
        if(!this.validate())
            return false;
        const data = {
            name : this.state.name || "---",
            email : this.state.email  || "---",
            mobile : this.state.mobile || "---",
            rating: this.state.rating < 0 ? 0 : this.state.rating,
            comment : this.state.comment || "---",
            brand : this.state.brand || "---"
        }
        this.props.middleware.postReview(data,response => {
            if(response.code)
                this.setState({
                    error : response.message
                })
            else
            this.setState({
                error : response
            })
        })
    }

    loginWithGoogle = () => {
        this.props.middleware.loginWithGoogle((response) => {
            if(response.code)
                return
            this.setState({
                operation : this.showPostReviewButton()
            })      
        })
    }

    showLoginButton = () => {
        return(
            <img 
                alt="google-signin-button" 
                src="../../../images/btn_google_signin_dark_normal_web@2x.png"
                onClick={this.loginWithGoogle}    
            />
        )
    }

    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                update={this.update}
                submit={this.submit}
            />
        )
    }
}

export default Container