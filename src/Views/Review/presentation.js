import React from 'react'
import './style.css'

import Input from '../../Components/Input'
import StarRating from '../../Components/StarRating';
import Signin from '../../Components/Signin'
import Select from '../../Components/Select'

import SlideShow from '../../Components/SlideShow'

class Presentation extends React.Component{
    render(){
        return(
            <div>
                <SlideShow slides={this.props.middleware.state.brands} />
                <div className="container default-review-card my-5">
                    <div className="card">
                        <div className="card-header text-center">
                            {this.props.error}
                        </div>
                        <div className="card-body">
                            <form onSubmit={this.props.submit}>
                                <Select inputName="Brand" options={this.props.options} error={this.props.brand_error} name="brand" defaultValue="" onChange={this.props.update} />
                                <Input inputName="Name" error={this.props.name_error} name="name" onChange={this.props.update}/>
                                <Input inputName="Email" error={this.props.email_error} name="email" onChange={this.props.update}/>
                                <Input inputName="Mobile Number" error={this.props.mobile_error} name="mobile" onChange={this.props.update}/>
                                <Input inputName="comment" name="comment" onChange={this.props.update}/>
                                <StarRating name="rating" error={this.props.rating_error} onChange={this.props.update}/>
                                <Signin middleware={this.props.middleware}/>
                                {this.props.operation}
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Presentation