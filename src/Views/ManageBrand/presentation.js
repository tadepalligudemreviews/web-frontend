import React from 'react'
import './style.css'

import Input from '../../Components/Input'
import Signin from '../../Components/Signin'
import Select from '../../Components/Select'

import Page404 from '../../Components/404'

class Presentation extends React.Component{
    render(){
        // console.log(this.props.newData)
        const email = this.props.middleware.state.auth.currentUser.email
        if (email !== "gopinadh5g7@sasi.ac.in" && email !== "naveen.loveall@gmail.com")
            return (
                <Page404 />
            )
        else
        return(
            <div className="container default-review-card my-5">
                <div className="card">
                    <div>{this.props.error}</div>
                    <div className="card-body">
                        <form onSubmit={this.props.submit}>
                            <Select inputName="Brand" options={this.props.options} error={this.props.brand_error} name="id" defaultValue="" onChange={this.props.update} />
                            <Input inputName="Brand Name" value={this.props.newData.name} error={this.props.name_error} name="name" onChange={this.props.update} disabled={this.props.formSubmitted}/>
                            <Input inputName="Brand Category" value={this.props.newData.category} error={this.props.category_error} name="category" onChange={this.props.update} disabled={this.props.formSubmitted}/>
                            <Input inputName="Brand Website" value={this.props.newData.website} name="website" onChange={this.props.update} disabled={this.props.formSubmitted}/>
                            <Input inputName="Brand Banner" error={this.props.banner_error} name="banner" type="file" onChange={this.props.update} disabled={this.props.formSubmitted}/>
                            <Input inputName="Promotion Active" checked={this.props.newData.isRunning} error={this.props.banner_error} name="isRunning" type="checkbox" onChange={this.props.update} disabled={this.props.formSubmitted}/>
                            <Signin middleware={this.props.middleware} disabled={this.props.formSubmitted}/>
                            {this.props.operation}
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default Presentation