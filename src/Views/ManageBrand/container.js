import React from 'react'
import Presentation from './presentation'

class Container extends React.Component{
    state = {}
    formSubmitted = false
    constructor(props){
        super(props)
        if(this.props.middleware.state.auth.currentUser){
            this.state.operation = this.showPostReviewButton()
        }
        this.state.options = this.generateBrandsJSX(this.props.middleware.state.brands)
    }

    generateBrandsJSX = (brandList) => {
        console.log(brandList)
        const jsx = brandList.filter(b => b.raw.id ? true : false).map(b => {
            return (
                <option key={b.raw.id} value={b.raw.id}>{b.raw.name} ({b.raw.category})</option>
            )
        })
        jsx.push((
            <option key="null_id" value="">---</option>
        ))
        return jsx
    }
    
    componentDidMount = () => {

    }

    showPostReviewButton = () => {
        return(
            <div className="text-center row m-3">
                <div className="col">
                    <button className="btn btn-outline-success btn-lg" disabled={this.formSubmitted}>UPDATE</button>
                </div>
                <div className="col">
                    <button className="btn btn-outline-warning btn-lg" type="reset" disabled={this.formSubmitted}>RESET</button>
                </div>

            </div>
        )
    }

    validate = (e) => {
        let flag = 1

        if(e){
            if ((e.target.name === "name" || e.target.name === "category") && !e.target.value.match(/^[A-z\s]+$/)){
                this.setState({
                    name_error: "INVALID NAME",
                    category_error : "INVALID CATEGORY"
                })
                flag = 0
            }

            if(e.target.name === "banner"){
                this.setState({
                    banner : e.target.files[0]
                })
            }
            this.formSubmitted = false
            return flag
        }

        if(this.state.name)
        if(!this.state.name.match(/^[A-z\s]+$/)){
            this.setState({
                name_error : "INVALID NAME"
            })
            flag = 0
        }

        if(this.state.category)
        if(!this.state.category.match(/^[A-z\s]+$/)){
            this.setState({
                category_error : "INVALID CATEGORY"
            })
            flag = 0
        }

        if(!this.state.id){
            this.setState({
                brand_error : "INVALID BRAND"
            })
            flag = 0
        }

        this.formSubmitted = false
        return flag
    }

    update = (e) => {
        if(e.target.name === "isRunning"){
            // console.log(e.target.checked)
            this.newData[e.target.name] = e.target.checked
            this.setState({
                isRunning : this.newData[e.target.name]
            })
        }
        else{
            this.newData[e.target.name] = e.target.value
            this.setState({
                [e.target.name] : e.target.value,
                name_error : "",
                category_error : "",
                banner_error : "",
                rating_error : "",
                brand_error : "",
                error : ""
            })
        }
        this.validate(e)
        
        if(e.target.name === "id")
            this.loadData(e)
    }
    newData = {}
    loadData = (e) => {
        const data = this.props.middleware.state.brands.filter(b => b.raw.id === e.target.value)
        if(data.length < 1)
            this.newData = {
                name : "",
                category : "",
                website : ""
            }
        else
            this.newData = {
                name : data[0].raw.name,
                category : data[0].raw.category,
                website : data[0].raw.website,
                isRunning : data[0].raw.isRunning
            }
        console.log(this.newData)
        this.setState({
            ...this.newData
        })
    }

    submit = (e) => {
        e.preventDefault();
        this.formSubmitted = true
        if(!this.validate())
            return false;
        this.formSubmitted = true
        const data = {
            name : this.state.name || "---",
            category : this.state.category  || "---",
            website : this.state.website || ("/"+this.state.name),
            isRunning : this.state.isRunning,
            id : this.state.id
        }
        if(!this.state.banner)
        this.props.middleware.manageBrand(data,this.callback)
        else{
            data.banner = this.state.banner
            this.props.middleware.manageBrandWithImage(data,this.callback)
        }
    }

    callback = (response) => {
        this.formSubmitted = false
        if(response.code){
            this.setState({
                error : response.message
            })
        }else{
            this.setState({
                error : "COMPLETED"
            })
        }
    }

    loginWithGoogle = () => {
        this.props.middleware.loginWithGoogle((response) => {
            if(response.code)
                return
            this.setState({
                operation : this.showPostReviewButton()
            })      
        })
    }

    showLoginButton = () => {
        return(
            <img 
                alt="google-signin-button" 
                src="../../../images/btn_google_signin_dark_normal_web@2x.png"
                onClick={this.loginWithGoogle}
            />
        )
    }

    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                update={this.update}
                submit={this.submit}
                formSubmitted={this.formSubmitted}
                newData={this.newData}
            />
        )
    }
}

export default Container