import firebase from 'firebase'

class Firebase{

    constructor(previousState){
        this.state = previousState
        this.state.firebase = firebase
        this.state.brands = [
            {
                brandName : "TEST",
                brandCategory : "TEST",
                imageUrl : 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/142996/paris.jpg',
                website : {
                    text : "developerswork.online",
                    link : "https://developerswork.online"
                },
                raw : {
                    id : 0,
                    name : "unknown",
                    category : "unknown"
                }
            }
        ]
    }

    setFirebaseConfiguration = function(){
        const firebaseConfig = {
            apiKey: "AIzaSyB3416HH6Q2Uw_wSHEMlRqwO1v5FTgNDLU",
            authDomain: "tadepalligudem-feedback.firebaseapp.com",
            databaseURL: "https://tadepalligudem-feedback.firebaseio.com",
            projectId: "tadepalligudem-feedback",
            storageBucket: "tadepalligudem-feedback.appspot.com",
            messagingSenderId: "882993222874",
            appId: "1:882993222874:web:a88c16e7c62b6486c381e3"
        };
        this.intailiseApp(firebaseConfig)
    }

    intailiseApp = (configuration) => {
        this.state.firebase.initializeApp(configuration)
        this.state.auth = this.state.firebase.auth()
        this.state.storage = this.state.firebase.storage().ref()
        this.state.firestore = this.state.firebase.firestore()

        this.loadData()

        this.state.auth.onAuthStateChanged((user) => {
            console.log(user)
            if (user) {
            } else {
                // callback(user);
            }
            this.state.mainCallback("UPDATE IN AUTH")
        });

        console.log(this.state)
    }

    loadData = () => {
        const oldData = this.state.brands
        this.state.brands = []
        return this.state.firestore.collection('BRANDS').where("isRunning","==",true).get()
        .then(docList => {
            docList.docs.map(doc => {
                const buffer = doc.data()
                console.log(buffer)
                const data = {
                    id : buffer.id,
                    brandName : buffer.name,
                    brandCategory : buffer.category,
                    imageUrl : buffer.banner,
                    website: {
                        text: buffer.website,
                        link: buffer.website
                    },
                    raw : buffer
                }
                return this.state.brands.push(data)
            })
            console.log(oldData)
            this.state.brands = this.state.brands.length > 0 ? this.state.brands : oldData
            console.log(this.state.brands)
            return this.state.mainCallback("UPDATE OF BRANDS")
        }).catch(err => {
            console.log(err)
        })
    }

    loginWithGoogle = (callback) => {
        const provider = new this.state.firebase.auth.GoogleAuthProvider();
        this.state.auth.signInWithPopup(provider)
        .then(res => {
            if(res.user)
                callback(res.user)
            else
                callback({
                    code : "auth/authentication-failed",
                    message : "Authentication failed"
                })
        }).catch(err => {
            callback(err)
        })
    }

    signOut = () => {
        return this.state.auth.signOut()
    }

    postReview = (data,callback) => {
        data.meta = {
            uid : this.state.auth.currentUser.uid,
            timestamp : new Date().toUTCString()
        }
        console.log(data)
        const docRef = this.state.firestore.collection('REVIEWS').doc()
        docRef.set(data).then(res => {
            console.log('DONE')
            callback("POSTED")
        }).catch(err => {
            console.log(err)
            callback(err)
        })
    }

    addBrand = (data,callback) => {
        console.log(data)
        let filename = data.banner.name + (new Date().toISOString()) + data.banner.name.split(".").slice(-1)[0];
        return this.state.storage.child("uploads/"+filename).put(data.banner)
        .then(res => {
            // console.log(res)
            data.source = res.metadata.fullPath
            return res.ref.getDownloadURL();
        }).then(res => {
            // data.sourceBanner = data.banner
            data.banner = res
            data.isRunning = true
            const docRef = this.state.firestore.collection("BRANDS").doc()
            // console.log(docRef)
            data.id = docRef.id
            return docRef.set(data)
        }).then(res => {
            console.log("DONE")
            callback({
                status : true
            })
        }).catch(err => {
            console.log(err)
            callback({
                code : "failed",
                message : "failed"
            })
        })
    }

    manageBrandWithImage = (data,callback) => {
        console.log(data)
        let filename = data.banner.name + (new Date().toISOString()) + data.banner.name.split(".").slice(-1)[0];
        return this.state.storage.child("uploads/"+filename).put(data.banner)
        .then(res => {
            // console.log(res)
            data.source = res.metadata.fullPath
            return res.ref.getDownloadURL();
        }).then(res => {
            // data.sourceBanner = data.banner
            data.banner = res
            // data.isRunning = true
            const docRef = this.state.firestore.collection("BRANDS").doc(data.id)
            // console.log(docRef)
            
            return docRef.set(data)
        }).then(res => {
            console.log("DONE")
            callback({
                status : true
            })
        }).catch(err => {
            console.log(err)
            callback({
                code : "failed",
                message : "failed"
            })
        })
    }

    manageBrand = (data,callback) => {
        console.log(data)
        const docRef = this.state.firestore.collection("BRANDS").doc(data.id)
        docRef.set(data,{merge:true})
        .then(res => {
            console.log("DONE")
            callback({
                status : true
            })
        }).catch(err => {
            console.log(err)
            callback({
                code : "failed",
                message : "failed"
            })
        })
    }


    getReviews = (id) => {
    
        return this.state.firestore.collection('REVIEWS').where("brand", "==", id).get()
        .then(docList => {
            // console.log(docList)
            const reviews = []
            docList.forEach(element => {
                const data = element.data();
                reviews.push(data)
            });
            return reviews
        }).catch(err => {
            return console.log(err)
        })
    }

}

export default Firebase