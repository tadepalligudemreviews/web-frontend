import Firebase from './firebase'


class Middleware{
    state = {}

    constructor(callback){

        this.state.mainCallback = callback

        const firebase = new Firebase(this.state)
        firebase.setFirebaseConfiguration()
        this.loginWithGoogle = firebase.loginWithGoogle
        this.postReview = firebase.postReview
        this.addBrand = firebase.addBrand
        this.signOut = firebase.signOut
        this.manageBrand = firebase.manageBrand
        this.manageBrandWithImage = firebase.manageBrandWithImage
        this.getReviews = firebase.getReviews
    }

}
export default Middleware