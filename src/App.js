import React from 'react';
import {BrowserRouter} from 'react-router-dom'

import Main from './main';

class App extends React.Component{

  render(){
    return (
      <BrowserRouter className="tpg-reviews">
        <Main/>
      </BrowserRouter>
    )
  }
}

export default App;
