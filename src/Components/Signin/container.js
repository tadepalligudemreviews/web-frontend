import React from 'react'
import Presentation from './presentation'
import './style.css'

class Container extends React.Component{
    constructor(props) {
        super(props)
        if (this.props.middleware.state.auth.currentUser) {
            this.state = {
                operation: this.showUserName()
            }
        } else {
            this.state = {
                operation: this.showLoginButton()
            }
        }
    }
    showLoginButton = () => {
        return (
            <img
                alt="google-signin-button"
                src="../../../images/signin-button.png"
                onClick={this.loginWithGoogle}
                className="sign-in-with-google"
            />
        )
    }

    showUserName = () => {
        return (
            <div>
                Signed in as: <a href="#logout" onClick={this.props.middleware.signOut}>{this.props.middleware.state.auth.currentUser.displayName}</a>
            </div>
        )
    }

    loginWithGoogle = () => {
        this.props.middleware.loginWithGoogle((response) => {
            if (response.code)
                return
            this.setState({
                operation: this.showUserName()
            })
        })
    }
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
            />
        )
    }
}

export default Container