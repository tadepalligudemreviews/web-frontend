import React from 'react'
import './style.css'

class Presentation extends React.Component{
    constructor(props){
        super(props)
        console.log(this.props)
        this.runAutochangeTO = this.props.runAutochangeTO
        this.changeSlides = this.props.changeSlides 
        this.classNames = this.props.classNames
    }

    render() {
        const activeSlide = this.props.activeSlide;
        const prevSlide = this.props.prevSlide;
        const sliderReady = this.props.sliderReady;
        return (
            <div className={this.classNames('slider', { 's--ready': sliderReady })}>
                <p className="slider__top-heading">PROMOTION</p>
                <div className="slider__slides">
                    {this.props.slides.map((slide, index) => (
                        <div
                            className={this.classNames('slider__slide', { 's--active': activeSlide === index, 's--prev': prevSlide === index })}
                            key={slide.id}
                        >
                            <div className="slider__slide-content">
                                <h3 className="slider__slide-subheading">{slide.brandCategory || slide.brandName}</h3>
                                <h2 className="slider__slide-heading">
                                    {slide.brandName.split('').map((l,index) => <span key={l+index}>{l}</span>)}
                                </h2>
                                <p className="slider__slide-readmore">
                                    <a href={slide.website.link} target={slide.website.link ? "_blank" : ""}>
                                        <button className="btn btn-outline-dark btn-lg btn-block" type="button">
                                            {slide.website.text}
                                        </button>
                                    </a>
                                </p>
                                <p className="slider__slide-readmore">
                                    <a href={"/view/"+slide.raw.id}>
                                        <button className="btn btn-success btn-lg btn-block" type="button">
                                            View Reviews
                                        </button>
                                    </a>
                                </p>
                            </div>
                            <div className="slider__slide-parts">
                                {[...Array(this.IMAGE_PARTS).fill()].map((x, i) => (
                                    <div className="slider__slide-part" key={i}>
                                        <div className="slider__slide-part-inner" style={{ backgroundImage: `url(${slide.imageUrl})` }} />
                                    </div>
                                ))}
                            </div>
                        </div>
                    ))}
                </div>
                <div className="slider__control" onClick={() => this.changeSlides(-1)} />
                <div className="slider__control slider__control--right" onClick={() => this.changeSlides(1)} />
            </div>
        );
    }
}

export default Presentation