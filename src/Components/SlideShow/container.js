import React from 'react'
import Presentation from './presentation'

class Container extends React.Component{
    state = {
        activeSlide: -1,
        prevSlide: -1,
        sliderReady: false
    }
    constructor(props) {
        super(props);
        this.IMAGE_PARTS = 4;
        this.changeTO = null;
        this.AUTOCHANGE_TIME = 4000;
    }

    componentWillUnmount() {
        window.clearTimeout(this.changeTO);
    }

    componentDidMount() {
        this.runAutochangeTO();
        setTimeout(() => {
            this.setState({ 
                activeSlide: 0, 
                sliderReady: true 
            });
        }, 0);
    }

    runAutochangeTO = () => {
        this.changeTO = setTimeout(() => {
            this.changeSlides(1);
            this.runAutochangeTO();
        }, this.AUTOCHANGE_TIME);
    }

    changeSlides = (change) => {
        window.clearTimeout(this.changeTO);
        const { length } = this.props.slides;
        const prevSlide = this.state.activeSlide;
        let activeSlide = prevSlide + change;
        if (activeSlide < 0) activeSlide = length - 1;
        if (activeSlide >= length) activeSlide = 0;
        this.setState({ activeSlide, prevSlide });
    }

    classNames = (main,adders) => {
        for(let i in adders){
            if(adders[i])
                main += " " + i
        }
        return main
    }

    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                runAutochangeTO={this.runAutochangeTO}
                changeSlides={this.changeSlides}  
                classNames={this.classNames}
            />
        )
    }
}

export default Container