import React from 'react'
import {Navbar} from 'react-bootstrap'
import Signin from '../Signin'

class Presentation extends React.Component{
    render(){
        return(
            <Navbar className="bg-white">
                <Navbar.Brand href="#main">
                    TADEPALLIGUDEM REVIEWS
                </Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                        <Signin middleware={this.props.middleware} />
                        {/* Signed in as: <a href="#login">Mark Otto</a> */}
                    </Navbar.Text>
                </Navbar.Collapse>
            </Navbar>
        )
    }
}

export default Presentation