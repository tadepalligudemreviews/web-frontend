import React from 'react'
import Presentation from './presentation'

class Container extends React.Component{
    data = {
        name : "gopinahd",
        comment : "working on the react",
        photoUrl: "https://lh3.googleusercontent.com/a-/AAuE7mDPj1lh6yhsAftM-tzV7rUcrzztGwpFMikVt04-",
        timestamp: "Sat, 21 Dec 2019 13:23:29 GMT",
        rating : 1
    }

    constructor(props){
        super(props)
        console.log(this.props.data)
        this.data = {...this.props.data}
    }
    
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                data={this.data}
            />
        )
    }
}

export default Container