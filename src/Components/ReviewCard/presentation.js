import React from 'react'

import './style.css'

import StarRating from '../StarRating'

class Presentation extends React.Component {
    render() {
        return (
            <div>
                <div className="courses-container text-center">
                    <div className="course">
                        <div className="course-preview">
                            <h6><img src={this.props.data.photoUrl} alt="user" width="200px"/></h6>
                            <h2>{this.props.data.name}</h2>
                            {/* <a href="#">View all chapters <i className="fas fa-chevron-right"></i></a> */}
                        </div>
                        <div className="course-info">
                            {/* <div className="progress-container">
                                <div className="progress"></div>
                                <span className="progress-text">
                                    6/9 Challenges
                                </span>
                            </div> */}
                            <h6>{this.props.data.timestamp}</h6>
                            <h2>{this.props.data.comment}</h2>
                            {/* <button className="btn">Continue</button> */}
                            <h3 className="center">
                                <StarRating value={this.props.data.rating} disabled={true} className="center" name="rating" error={this.props.rating_error} onChange={this.props.update} />
                            </h3>
                        </div>
                        
                    </div>
                </div>

                {/* <div class="social-panel-container">
                    <div class="social-panel">
                        <p>Created with <i class="fa fa-heart"></i> by
            <a target="_blank" href="https://florin-pop.com">Florin Pop</a></p>
                        <button class="close-btn"><i class="fas fa-times"></i></button>
                        <h4>Get in touch on</h4>
                        <ul>
                            <li>
                                <a href="https://www.patreon.com/florinpop17" target="_blank">
                                    <i class="fab fa-discord"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://twitter.com/florinpop1705" target="_blank">
                                    <i class="fab fa-twitter"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://linkedin.com/in/florinpop17" target="_blank">
                                    <i class="fab fa-linkedin"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://facebook.com/florinpop17" target="_blank">
                                    <i class="fab fa-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="https://instagram.com/florinpop17" target="_blank">
                                    <i class="fab fa-instagram"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <button class="floating-btn">
                        Get in Touch
                    </button>

                <div class="floating-text">
                    Part of <a href="https://florin-pop.com/blog/2019/09/100-days-100-projects" target="_blank">#100Days100Projects</a>
                </div> */}
            </div>

        )
    }
}

export default Presentation