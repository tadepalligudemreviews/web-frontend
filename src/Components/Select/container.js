import React from 'react'
import Presentation from './presentation'

class Container extends React.Component{

    state = {}
    componentDidMount(){
        console.log(this.props)
    }

    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
            />
        )
    }
}

export default Container