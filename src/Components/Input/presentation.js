import React from 'react'

class Presentation extends React.Component{
    render(){
        return(
            <div className="input-group m-3 p-2">
                <div className="input-group-prepend">
                    <div className="input-group-text">{this.props.inputName}</div>
                </div>
                <input 
                    className={"form-control "+ (this.props.error ? "is-invalid" : "")}
                    type={this.props.type} 
                    name={this.props.name} 
                    onChange={this.props.onChange} 
                    onClick={this.props.onClick}
                    placeholder={this.props.placeholder}
                    value={this.props.value}
                    disabled={this.props.disabled}
                    checked={this.props.checked}
                />
                <div className="invalid-feedback">
                    {this.props.error}
                </div>
            </div>
        )
    }
}

export default Presentation