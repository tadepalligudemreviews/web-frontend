import React from 'react'
import Presentation from './presentation'

class Container extends React.Component{
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
            />
        )
    }
}

export default Container