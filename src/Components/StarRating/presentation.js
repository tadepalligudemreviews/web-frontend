import React from 'react'
import './style.css'

class Presentation extends React.Component{
    constructor(props){
        super(props)
        this.star_out = this.props.star_out
        this.star_over = this.props.star_over
        this.rate = this.props.rate
    }
    // componentDidUpdate(){
    //     console.log(this.state)
    // }
    render() {
        // console.log(this.state)
        var stars = [];

        for (var i = 0; i < 5; i++) {
            var klass = 'star-rating__star';

            if (this.props.temp >= i) {
                klass += ' star-rating__is-selected';
            }
            if(this.props.disabled)
                klass += ' star-rating__is-disabled';


            stars.push(
                <label
                    className={klass}
                    key={i}
                    id={i}
                    onClick={this.rate}
                    onMouseOver={this.star_over}
                    onMouseOut={this.star_out}>
                    ★
                </label>
            );
        }

        return (
            <div className={this.props.className ? this.props.className : "star-rating input-group m-3 p-2 "} >
                {stars}
            </div>
        );
    }
}

export default Presentation