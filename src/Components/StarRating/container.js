import React from 'react'
import Presentation from './presentation'

class Container extends React.Component{
    state = {
        value: -1,
        temp: -1
    }
    constructor(props){
        super(props)
        this.state = {
            value : this.props.value-1 || -1,
            temp : this.props.value-1 || -1,
            name : this.props.name || "starrating",
            disabled : this.props.disabled ? true : false
        }
        // this.props.onChange = this.props.onChange | null
    }
    rate = (e) => {
        if (this.state.disabled)
            return;
        const rating = parseInt(e.target.id)
        console.log(rating)
        this.setState({
            value : rating,
            temp : rating
        });
        if(this.props.onChange)
        this.props.onChange({
            target : {
                name : this.state.name,
                value : rating
            }
        })
    }
    star_over = (e) => {
        if(this.state.disabled)
            return;
        // console.log(e)
        const rating = e.target.id
        // console.log(rating)
        this.setState({
            temp : rating,
        });
    }
    star_out = (e) => {
        // const rating = e.target.key
        // console.log(this.state)
        this.setState({ temp : this.state.value });
    }
    render(){
        return(
            <Presentation
                {...this.props}
                {...this.state}
                star_out={this.star_out}
                star_over={this.star_over}
                rate={this.rate}
            />
        )
    }
}

export default Container