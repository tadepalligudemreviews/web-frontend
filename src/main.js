import React from 'react'

// import Input from './Components/Input'

import Middleware from './Middleware'

import Review from './Views/Review';
import SlideShow from './Components/SlideShow';
import NavBar from './Components/NavBar'

import AddBrand from './Views/AddBrand'
import ManageBrand from './Views/ManageBrand'
import BrandReviews from './Views/BrandReviews';

import Page404 from './Components/404'

import {BrowserRouter,Route, Switch, Redirect} from 'react-router-dom'
import Footer from './Components/Footer';

class Main extends React.Component {
    state = {}
    constructor(props){
        super(props)
        this.middleware = new Middleware(this.listener)
    }

    listener = (response) => {
        console.log(response)
        this.setState({
            update : response
        })
    }

    render(){
        console.log(this.middleware.state)
        if(!this.middleware.state.firebase)
        return(
            <BrowserRouter>
                <SlideShow slides={this.middleware.state.brands} />
                <Footer />
            </BrowserRouter>
        )
        else if(this.middleware.state.auth.currentUser)
        return(
            <BrowserRouter>
                <NavBar middleware={this.middleware} />
                <Switch>
                    <Route exact path="/" component={() => <Review middleware={this.middleware} />} />
                    <Route path="/add" component={() => <AddBrand middleware={this.middleware} />} />
                    <Route path="/manage" component={() => <ManageBrand middleware={this.middleware} />} />
                    <Route path="/view/:brand" component={() => <BrandReviews middleware={this.middleware} />} />
                    <Route path="**" component={() => <Page404 middleware={this.middleware} />} />
                    <Redirect from="**" to="/" />
                </Switch>
                <Footer />
            </BrowserRouter>
        )
        else
        return(
            <BrowserRouter>
                <NavBar middleware={this.middleware} />
                <Switch>
                    <Route exact path="/" component={() => <SlideShow slides={this.middleware.state.brands} />} />
                    <Route path="/view/:brand" component={() => <BrandReviews middleware={this.middleware} />} />
                    <Route path="**" component={() => <Page404 middleware={this.middleware} />} />
                    <Redirect from="**" to="/" />
                </Switch>
                <Footer />
            </BrowserRouter>
        )
    }
}

export default Main